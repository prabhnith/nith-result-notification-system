import sys
import xlwt
import argparse

from datetime import date
from selenium import webdriver
from pyvirtualdisplay import Display
from selenium.common.exceptions import NoSuchElementException

display = Display(visible=0, size=(800, 600))
display.start()

wb= xlwt.Workbook()
ws= wb.add_sheet("sheet1")
browser = webdriver.Chrome()


def fileName(startRoll):
	if startRoll[2] == '1':
		return 'civil_Result'
	elif startRoll[2] == '2':
		return 'EEE_Result'	
	elif startRoll[2] == '3':
		return 'ME_Result'	
	elif startRoll[2] == '4':
		return 'ECE_Result'	
	elif startRoll[2] == '5':
		return 'CSE_Result'	
	elif startRoll[2] == '7':
		return 'CHE_Result'


def deco(filename):
	print("Showing the " + filename + " and saving on local drive...")
	browser.get('http://nith.ac.in/')
	print(browser.title)
	browser.find_element_by_xpath("//a[@href='http://14.139.56.15/result.htm']").click()
	print(browser.title)


count=0
def fetchResult(sem,startRoll):
	deco(fileName(startRoll))
	for i,RollNum in zip(range(100), range(int(startRoll),int(startRoll)+100)):
		browser.get('https://14.139.56.15/scheme'+startRoll[:2]+'/studentresult/index.asp')
		browser.find_element_by_xpath("//input[@name='RollNumber']").send_keys(RollNum)
		browser.find_element_by_xpath("//input[@value='Submit']").click()
		
		try:
			try:
				studentName = browser.find_element_by_xpath("html/body/div[1]/table/tbody/tr[1]/td[2]/div").text
			except :
				break
			ws.write(i,0,RollNum)
			ws.write(i,1,studentName)
			
			studentCGPI = browser.find_element_by_xpath("html/body/div["+str(2*int(sem)+1)+"]/table/tbody/tr[2]/td[3]").text
			studentCGPI = studentCGPI.split('=')[1]
			ws.write(i,2,studentCGPI)
			
			studentSGPI = browser.find_element_by_xpath("html/body/div["+str(2*int(sem)+1)+"]/table/tbody/tr[2]/td[1]").text
			studentSGPI = studentSGPI.split('=')[1]
			ws.write(i,3,studentSGPI)
			# print(RollNum," ",studentName," ",studentCGPI," ",studentSGPI)
			print('{:<6} {:<25}{:<9}{:<5}'.format(RollNum,studentName,studentCGPI,studentSGPI))

		except NoSuchElementException:
			count=count+1
			if(count>10):
				break
			print("Student result is not avaiable.")

	wb.save(fileName(startRoll) +"_sem"+sem+"_"+str(date.today().year) +".xls")

def main():
	print("-----------------------------------------------------------------")
	print("| This is the script to store the NITH result of branch locally  |")
	print("-----------------------------------------------------------------")

	fetchResult(input("Enter the semester : "),\
				input("Enter starting Rollnum of the desired branch :"))
	display.stop()

if __name__ == "__main__":
   main()