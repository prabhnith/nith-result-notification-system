import sys
import argparse

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from pyvirtualdisplay import Display

display = Display(visible=0, size=(800, 600))
display.start()
browser = webdriver.Chrome()

message 		=''
phoneNumber		=''
studentRollNum	=''


def data(mob,roll):
	if(len(mob)!=10):
		print("Enter 10 digit mobile number.")
		sys.exit(1)

	if(len(roll)!=5):
		print("Enter 5 digit roll number.")
		sys.exit(1)

	global phoneNumber, studentRollNum
	phoneNumber    = mob
	studentRollNum = roll


def fetchResult():
	try:
		browser.get('http://nith.ac.in/')
		print(browser.title)
		browser.find_element_by_xpath("//a[@href='http://14.139.56.15/result.htm']").click()
		print(browser.title)

		browser.find_element_by_xpath("//a[@href='https://14.139.56.15/scheme13/studentresult/index.asp']").click()
		# print("Sem6 clicked.")
		browser.find_element_by_xpath("//input[@name='RollNumber']").send_keys(studentRollNum)
		# print("Rollnum filled.")
		browser.find_element_by_xpath("//input[@value='Submit']").click()
		# print("B1 clicked.")
		# print(browser.title)
		studentName = browser.find_element_by_xpath("html/body/div[1]/table/tbody/tr[1]/td[2]/div").text
		studentCGPI = browser.find_element_by_xpath("html/body/div[13]/table/tbody/tr[2]/td[3]").text
		studentSGPI = browser.find_element_by_xpath("html/body/div[13]/table/tbody/tr[2]/td[1]").text
		# browser.close()
	except NoSuchElementException:
		print("Result has been removed from the site.")
		sys.exit(1)

	global message
	message = 'Your Result\nName       : %s\nRollNumber : %s\nSGPI : %s\nCGPI : %s' %(studentName,studentRollNum,studentSGPI,studentCGPI)
	print(message)


def sendSMS():
	print("Sending SMS to %s..." %phoneNumber)
	# browser = webdriver.Firefox()		
	browser.get('http://site21.way2sms.com/content/index.html')
	username, password = "",""
	# username, password = credentials.credentials()
	browser.find_element_by_id("username").send_keys(username)
	browser.find_element_by_id("password").send_keys(password)
	browser.find_element_by_id("loginBTN").click()
	browser.find_element_by_xpath("//input[@value='Send Free SMS']").click()

	while True:
	    try:
	        browser.find_element_by_id("sendSMS").click()
	        browser.switch_to_frame("frame")
	        browser.find_element_by_id("mobile").send_keys(phoneNumber)
	        browser.find_element_by_id("message").send_keys(message)
	        browser.find_element_by_id("Send").click()
	        print("Message sent successfully.")
	        browser.close()
	        break
	    except NoSuchElementException:
	        browser.switch_to_default_content()
	        browser.close()
	        print("No element Found")


def main(argv):
	desc = '*'*5+"\nThis is the script for sending the NITH sem-7 result of given roll number to the phone number.\n"+'*'*5
	parser = argparse.ArgumentParser(description=desc)
	parser.add_argument('-m','--mob', help='-m : mobile number to which you want to send result',required=True)
	parser.add_argument('-r','--roll',help='-r : roll number of the student', required=True)

	args = parser.parse_args()

	data(args.mob, args.roll)
	fetchResult()
	sendSMS()

	display.stop()

if __name__ == "__main__":
   main(sys.argv[1:])