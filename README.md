# README #

This repo contains the code for the notification system that will send the notification 
to the students of the college when it will arrive on the site via SMS.

### What is this repository for? ###

* For the notifiation of result to respective students
* code for scraping the website to get the details of all the students
and store the information in the worksheet.
* helps in making and maintaining the class record. 

### How do I get set up? ###

* clone the repository
* set up way2sms account
* put the details in the code where mobile and password is mentioned.
* run the code with python

### Contribution guidelines ###

* Adding more feature to the code is welcome
* this code uses selenium, updating the code will be good

#### Update####
* This version of selenium didn't work with the chrome driver, so you need to update it.